<?php

namespace Tests\Unit;

use App\Services\CarRentalPriceCalculator;
use App\Services\Validators\CarRentalTimePeriodValidator;
use Tests\TestCase;

class CarRentalPriceCalculatorTest extends TestCase
{
    /** @var CarRentalPriceCalculator */
    private $carRentalPriceCalculator;

    public function setUp(): void
    {
        parent::setUp();
        $mockedCarRentalTimePeriodValidator = \Mockery::mock(CarRentalTimePeriodValidator::class);
        app()->instance(CarRentalTimePeriodValidator::class, $mockedCarRentalTimePeriodValidator);
        $mockedCarRentalTimePeriodValidator->shouldReceive("validate")->once()->andReturn(true);
        $this->carRentalPriceCalculator = new CarRentalPriceCalculator($mockedCarRentalTimePeriodValidator);
    }

    /**
     *
     * @return void
     */
    public function testLessThanEightHours()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-06 07:00:00";
        $desiredPrice = 30;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }

    /**
     *
     * @return void
     */
    public function testBetweenOneAndTwoDays()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-07 06:00:00";
        $desiredPrice = 220;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }

    /**
     *
     * @return void
     */
    public function testBetweenTwoAndThreeDays()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-08 08:00:00";
        $desiredPrice = 372;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }

    /**
     *
     * @return void
     */
    public function testBetweenThreeAndFourDays()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-09 09:05:00";
        $desiredPrice = 492;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }

    /**
     *
     * @return void
     */
    public function testBetweenFourAndFiveDays()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-10 10:07:00";
        $desiredPrice = 583;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }


    /**
     *
     * @return void
     */
    public function testMoreThanFiveDays()
    {
        $startDate = "2020-12-06 04:00:00";
        $endDate = "2020-12-14 11:07:00";
        $desiredPrice = 825.5;
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        $this->assertTrue($result["isValid"]);
        $this->assertEquals($desiredPrice, $result['data']);
    }
}
