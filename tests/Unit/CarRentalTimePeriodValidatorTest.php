<?php

namespace Tests\Unit;

use App\Services\Validators\CarRentalTimePeriodValidator;
use Tests\TestCase;

class CarRentalTimePeriodValidatorTest extends TestCase
{

    /** @var CarRentalTimePeriodValidator */
    private $carRentalTimePeriodValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->carRentalTimePeriodValidator = new CarRentalTimePeriodValidator();
    }

    /**
     *
     * @return void
     */
    public function testLessThanTwoHoursDiffShouldReturnFalse()
    {
        $startDate = "2020-12-06 10:00:00";
        $endDate = "2020-12-06 11:00:00";

        $this->assertFalse(
            $this->carRentalTimePeriodValidator->validate($startDate, $endDate)
        );
    }

    /**
     *
     * @return void
     */
    public function testMoreThanTenDaysDiffShouldReturnFalse()
    {
        $startDate = "2020-12-06 10:00:00";
        $endDate = "2020-12-17 11:00:00";

        $this->assertFalse(
            $this->carRentalTimePeriodValidator->validate($startDate, $endDate)
        );
    }


    /**
     *
     * @return void
     */
    public function testLessThanTenDaysAndMoreThanTwoHoursDiffShouldReturnTrue()
    {
        $startDate = "2020-12-06 10:00:00";
        $endDate = "2020-12-15 11:00:00";

        $this->assertTrue(
            $this->carRentalTimePeriodValidator->validate($startDate, $endDate)
        );
    }
}
