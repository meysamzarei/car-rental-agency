<?php

namespace Tests\Unit;

use App\Http\Requests\CarRequest;
use PHPUnit\Framework\TestCase;


class CarRequestTest extends TestCase
{

    private $carRequest;

    public function setUp(): void
    {
        parent::setUp();


        $this->carRequest = new CarRequest();
    }

    public function testCarStoreRequest()
    {
        $this->assertEquals([
            'license_plate' => 'required|regex:/[0-9]{1,3}-[A-Z]{1,2}-[0-9]{1,3}/i',
            'model' => 'required',
            'gas_tank_capacity' => 'required'
        ],
            $this->carRequest->rules()
        );
    }


}
