<?php

namespace Tests\Feature;

use App\Models\Car;
use App\Models\CarRental;
use App\Models\User;
use App\Services\Validators\CarRentalAllowanceValidator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\factory;

class CarRentalAllowanceTest extends TestCase
{
    use RefreshDatabase;

    /** @var CarRentalAllowanceValidator */
    private $carRentalAllowanceValidator;

    public function setUp(): void
    {
        parent::setUp();
        $this->carRentalAllowanceValidator = new CarRentalAllowanceValidator();
    }

    /**
     *
     * @return void
     */

    public function testAllowRentNotRentedCar()
    {

        $carRental = CarRental::factory()->notRented()->make();
        $carRental->save();

        $this->assertTrue(
            $this->carRentalAllowanceValidator->validate($carRental->car()->first())
        );

    }
    public function withoutAuthorization()
    {
        Gate::before(function () {
            return true;
        });

        return $this;
    }

    /**
     *
     * @return void
     */

    public function testNotAllowRentRentedCar()
    {
        $carRental = CarRental::factory()->rented()->make();
        $carRental->save();

        // logical test
        $this->assertFalse(
            $this->carRentalAllowanceValidator->validate($carRental->car()->first())
        );

        $response = $this->withoutAuthorization()
            ->withoutMiddleware(backpack_middleware())
            ->get('/admin/carrental/'.$carRental->car_id.'/create');

        // it should redirect to avoid rent a car
        $response->assertStatus(302);

    }
}
