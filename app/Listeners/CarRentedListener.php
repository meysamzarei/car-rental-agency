<?php

namespace App\Listeners;

use App\Events\CarRented;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CarRentedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CarRented  $event
     * @return void
     */
    public function handle(CarRented $event)
    {
        $car = $event->carRental->car()->first();
        $car->isRented = true;
        $car->save();
    }
}
