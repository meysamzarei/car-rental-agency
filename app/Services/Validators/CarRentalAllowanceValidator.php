<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 12/9/20
 * Time: 11:58 AM
 */

namespace App\Services\Validators;


use App\Models\Car;

class CarRentalAllowanceValidator
{

    public function validate(Car $car){
        return $car->activeRental() == null;
    }

}