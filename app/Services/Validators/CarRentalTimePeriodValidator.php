<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 12/5/20
 * Time: 10:34 AM
 */

namespace App\Services\Validators;


use DateTime;

class CarRentalTimePeriodValidator
{
    public function validate($startDate, $endDate){
        $startDate = new DateTime($startDate);
        $endDate = $startDate->diff(new DateTime($endDate));
        if ($endDate->h < config('global.rental_min_hours') && $endDate->days ==0)
            return false;
        elseif ($endDate->days > config('global.rental_max_days'))
            return false;
        else
            return true;
    }
}