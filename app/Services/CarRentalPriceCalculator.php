<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 12/4/20
 * Time: 1:03 PM
 */

namespace App\Services;


use App\Services\Validators\CarRentalTimePeriodValidator;

class CarRentalPriceCalculator
{
    private $carRentalTimePeriodValidator;

    /**
     * CarRentalPriceCalculator constructor.
     */
    public function __construct(CarRentalTimePeriodValidator $carRentalTimePeriodValidator)
    {
        $this->carRentalTimePeriodValidator = $carRentalTimePeriodValidator;
    }

    public function execute($startDate, $endDate){
        $response = [];
        if (!$this->carRentalTimePeriodValidator->validate($startDate, $endDate))
        {
            $response['isValid'] = false;
            $response['data'] = "Rental time should be more than 2 hours and less than 10 days.";

            return $response;
        }


        $factors = [];
        $factors = $this->prepareCalculationFactors($factors);

        $hours = $this->getRentalHours($startDate, $endDate);
        $separatedRentalHours = $this->calculateSeparatedRentalHours($factors, $hours);

        $response['isValid'] = true;
        $response['data'] = $this->calculateRentalPrice($separatedRentalHours, $factors);

        return $response;

    }

    /**
     * @param $factors
     * @return mixed
     */
    private function prepareCalculationFactors($factors)
    {
        $factors[0]['division'] = 8;
        $factors[0]['duration'] = 8;
        $factors[0]['unitPrice'] = 10;

        $factors[1]['division'] = 24;
        $factors[1]['duration'] = 16;
        $factors[1]['unitPrice'] = 8;


        $factors[2]['division'] = 48;
        $factors[2]['duration'] = 24;
        $factors[2]['unitPrice'] = 6;


        $factors[3]['division'] = 72;
        $factors[3]['duration'] = 24;
        $factors[3]['unitPrice'] = 5;


        $factors[4]['division'] = 96;
        $factors[4]['duration'] = 24;
        $factors[4]['unitPrice'] = 4;

        $factors[5]['division'] = 120;
        $factors[5]['duration'] = 24;
        $factors[5]['unitPrice'] = 2.5;
        return $factors;
    }

    /**
     * @param $factors
     * @param $hours
     * @return array
     */
    private function calculateSeparatedRentalHours($factors, $hours)
    {
        $hoursInDay = 24;
        $lastY = 0;
        $rentalHours = [];
        $pointer = 0;
        $max = count($factors) - 1;
        for ($i = 0; $i <= $max; $i++) $rentalHours[$i] = 0;

        if ($hours <  $factors[0]['division'])
        {
            $rentalHours[0] = $hours;
            return $rentalHours;
        }
        do {

            if ($pointer >= $max) {
                $duration = $hoursInDay;
                $division = ($pointer * $duration);

                $x = $hours / $division;
                $y = $hours % $division;
                if ($x >= 1) {
                    $rentalHours[$max] += $duration;
                    $lastY = $y;
                } else {
                    $rentalHours[$max] += $lastY;

                }
            } else {
                $x = $hours / $factors[$pointer]['division'];
                $y = $hours % $factors[$pointer]['division'];

                if ($x >= 1) {
                    $rentalHours[$pointer] = $factors[$pointer]['duration'];
                    $lastY = $y;
                } else {
                    if ($factors[$pointer]['duration'] < $hoursInDay && $pointer > 0)
                        $rentalHours[$pointer] = $lastY + $factors[$pointer - 1]['duration'];
                    else
                        $rentalHours[$pointer] = $lastY;
                }
            }

            $pointer++;
        } while ($x >= 1);
        return $rentalHours;
    }

    /**
     * @param $separatedRentalHours
     * @param $factors
     * @return float|int
     */
    private function calculateRentalPrice($separatedRentalHours, $factors)
    {
        $price = 0;
        foreach ($separatedRentalHours as $key => $result) {
            $price += $factors[$key]['unitPrice'] * $result;
        }
        return $price;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return float|int
     */
    private function getRentalHours($startDate, $endDate)
    {
        $timestamp1 = strtotime($startDate);
        $timestamp2 = strtotime($endDate);

        $hours = round(abs($timestamp2 - $timestamp1) / (60 * 60));
        return $hours;
    }
}