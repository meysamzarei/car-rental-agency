<?php

namespace App\Events;

use App\Models\Car;
use App\Models\CarRental;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CarRented
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $carRental;

    /**
     *
     * @return void
     */
    public function __construct(CarRental $carRental)
    {
        $this->carRental = $carRental;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('rental-cars');
    }
}
