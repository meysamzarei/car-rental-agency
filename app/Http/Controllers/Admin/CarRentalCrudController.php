<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarRentalRequest;
use App\Models\Car;
use App\Services\CarRentalPriceCalculator;
use App\Services\Validators\CarRentalAllowanceValidator;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class CarRentalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CarRentalCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    private $carRentalPriceCalculator;
    private $carRentalAllowanceValidator;
    private $car;

    /**
     * CarRentalCrudController constructor.
     * @param CarRentalPriceCalculator $calculator
     * @param CarRentalAllowanceValidator $carRentalAllowanceValidator
     */
    public function __construct(CarRentalPriceCalculator $calculator, CarRentalAllowanceValidator $carRentalAllowanceValidator)
    {
        parent::__construct();
        $this->carRentalPriceCalculator = $calculator;
        $this->carRentalAllowanceValidator = $carRentalAllowanceValidator;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $carId = \Route::current()->parameter('carId');
        $this->car = Car::find($carId);
        $this->validateCar();

        CRUD::setModel(\App\Models\CarRental::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/carrental/'.$this->car->id);
        CRUD::setEntityNameStrings($this->getSingularPageTitle(), $this->getPluralPageTitle());
        $this->crud->denyAccess("list");
    }


    protected function setupListOperation()
    {
        CRUD::setFromDb();

    }


    protected function setupCreateOperation()
    {

        CRUD::setValidation(CarRentalRequest::class);
        CRUD::setFromDb();
        $this->configureTheFields();
        $this->data['widgets']['after_content'] = [
            [
                'type' => 'custom_script'
            ]
        ];
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function getSingularPageTitle()
    {
        return $this->car->model . ' Rental';
    }


    private function getPluralPageTitle()
    {
        return $this->car->model . ' Rentals';
    }

    public function fetchCustomer()
    {
        return $this->fetch(\App\Models\Customer::class);
    }

    public function carRentalPriceCalculator($carId, $startDate, $endDate){
        $result = $this->carRentalPriceCalculator->execute($startDate, $endDate);
        if ($result['isValid'])
            $httpStatusCode = 200;
        else
            $httpStatusCode = 400;
        return response($result['data'], $httpStatusCode)
            ->header('Content-Type', 'text/plain');
    }

    private function configureTheFields()
    {
        $this->crud->modifyField("car_id", [
            'type' => 'hidden',
            'default' => $this->car->id
        ]);
        $this->crud->modifyField("isActive", [
            'type' => 'hidden',
            'default' => true
        ]);
        $this->crud->modifyField('customer_id', [

            'type' => "relationship",
            'ajax' => true,
            'inline_create' => true,
            'entity' => 'customer',
            'attribute' => 'name',
            'model' => 'App\Models\Customer',
            'data_source' => url($this->crud->getRoute() . "/fetch/customer")

        ]);
        $this->crud->modifyField('start_date', [
            'attributes' => [
                'id' => 'start_date'
            ]
        ]);
        $this->crud->modifyField('end_date', [
            'attributes' => [
                'id' => 'end_date'
            ]
        ]);
        $this->crud->modifyField('price', [
            'hint' => 'It will be calculated after changing the end date.',
            'attributes' => [
                'id' => 'price',
                'readonly' => 'readonly'
            ]
        ]);
    }

    /**
     * @return array|void
     */
    private function validateCar(): void
    {
        if ($this->car == null)
            throw new HttpResponseException(redirect('admin/car'));

        if (!$this->carRentalAllowanceValidator->validate($this->car)) {
            \Alert::success("This car is not allowed to be rented, since it is currently rented")->flash();
            throw new HttpResponseException(redirect('admin/car'));
        }
    }


}
