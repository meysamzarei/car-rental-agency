<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\CarRental;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarRentalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CarRental::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'car_id' => Car::factory()->rented(),
            'customer_id' => Customer::factory(),
            'start_date' => $this->faker->dateTime,
            'end_date' => $this->faker->dateTime,
            'price' => $this->faker->randomDigit,
            'isActive' => false,
        ];
    }

    public function rented()
    {
        return $this->state(function (array $attributes) {
            return [
                'car_id' => Car::factory()->rented(),
                'isActive' => true
            ];
        });
    }


    public function notRented()
    {
        return $this->state(function (array $attributes) {
            return [
                'car_id' => Car::factory()->notRented(),
                'isActive' => false
            ];
        });
    }
}
