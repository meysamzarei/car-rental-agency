<?php

namespace Database\Factories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'model' => $this->faker->name,
            'license_plate' => $this->faker->name,
            'gas_tank_capacity' => $this->faker->randomDigit.$this->faker->name,
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
            'isRented' => false

        ];
    }

    public function rented()
    {
        return $this->state(function (array $attributes) {
            return [
                'isRented' => true,
            ];
        });
    }

    public function notRented()
    {
        return $this->state(function (array $attributes) {
            return [
                'isRented' => false,
            ];
        });
    }
}
