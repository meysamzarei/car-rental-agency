<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    var CURRENT_URL = window.location.href;
</script>
@push('after_scripts')
    <script src="{{ asset('js/custom_scripts.js') }}"></script>
@endpush
