
@if ($crud->hasAccess('list'))
 @if($entry->isRented)
    <span style="color: red;">
        <br> Rented by {{$entry->activeRental()->getCustomerName()}} from {{$entry->activeRental()->start_date}} to {{ $entry->activeRental()->end_date }}

        </span>
 @else
     <a href="
 {{ url('admin/carrental/'.$entry->getKey().'/create') }}" class="btn btn-sm btn-link"><i class="la la-link"></i>Rent</a>
 @endif

@endif
